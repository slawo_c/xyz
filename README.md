# Challenge
 
The system is composed of 3 components which are designed to run as microservices.
The contracts for the services are declared in `proto`. The api service uses the REST version of contracts from the `users`` and `projects` services and transmits the calls directly to those services.
 
## Deployment
 
GKE has been chosen as the underlying platform.
 
The `manifests` dir contains the manifests to deploy the project to a kubernetes cluster. They require 2 secrets for the database connection.
 
The application is deployed at `http://34.107.147.171` The swagger ui is available at `http://34.107.147.171/swagger-ui/`.
 
The `v1/users/*` endpoints are freely accessible. To access the rest of the application a token is required. The token is emitted on successful authentication.
 
## Options
All services expose their flags when called with `--help`. In the manifests, options are setup using env variables.
 
## Build
All docker containers are built using multistage builds. The build process can be triggered through `make build`.
 
## api
The api service only aggregates the publicly exposed routes and proxies the rest calls to the relevant services over grpc.
 
## users
The users service is in charge of the storage and authentication of users. User ids are generated from the given emails
 
 
## Improvements
 
### Security
The cluster should not be accessible over HTTP, only HTTPS.

### Backups
No backup strategy is in place in this project. On should be set to ensure no data will be lost in case of a database crash.
 
### Auth
Separating auth from the `users` service would allow the authentication service to operate on a different isolated namespace with its own set of secrets and rules.
 
The naive implementation of tokens should be replaced with asymmetric JWT. With rolling keys issued by a dedicated auth service and periodically retrieved by the other trusted services.
 
### Instrumentation
Instrumenting the services to monitor the endpoints, latency, and other metrics is necessary before going to production. 
Monitoring routines with health reporting should be added on all DB connections and cross-service communications to catch issues before the users are heavily impacted.
 
### API
Replacing `api` with a graphQL endpoint would allow for a better separation of concerns across the services and data retrieval would be greatly simplified and easily addapted to the needs of frontend applications.
 
### users
In the future user ids should be generated using another method.
 
 
### devices
Currently devices are handled from the `projects` service. If devices needed to grow in complexity we should move them to their own service(s) to reduce knowledge about devices within the projects.
 
### serverless
This approach has not been explored.
 
 
## Access

### Register a new user

```
curl -XPOST -H "Content-Type: application/json" --data '{"email":"user@test.com", "password":"DONOTSHOW", "firstName":"John", "LastName":"Snow"}'  http://34.107.147.171/v1/users/register 
```
should return an answe in the form `{"user_id":"08c5de1b-3911-5e8d-a2e6-1e14cd74d906"}` on first run.


### Login user

```
curl -XPOST -H "Content-Type: application/json" --data '{"email":"user@test.com", "password":"DONOTSHOW"}'  http://34.107.147.171/v1/users/authenticate         
```
should return an answe in the form `{"token":"Bearer 08c5de1b-3911-5e8d-a2e6-1e14cd74d906"}`. The bearer token should be used in all subsequent calls to other API paths on http://34.107.147.171/swagger-ui.

ie:

```
curl -XPOST -H "Content-Type: application/json" -H "Authorization: Bearer 08c5de1b-3911-5e8d-a2e6-1e14cd74d906"  --data '{"name":"BETTER PROJECT"}'  http://34.107.147.171/v1/projects
```
