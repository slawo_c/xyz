PROJECTS = api users projects
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || cat $(CURDIR)/.version 2>/dev/null || echo v0)



.PHONY: version
version:
	@echo $(VERSION)

.PHONY: build
build: build-api build-users build-projects

build-%:
	docker build --build-arg VERSION=$(VERSION) -t gcr.io/iconic-exchange-272308/xyz/$*:$(VERSION) -f scripts/Dockerfile.$* .

.PHONY: proto
proto:
	./scripts/gen.sh

deploy: deploy-api deploy-users deploy-projects

deploy-%:
	kubectl set image deployment/$* $*=gcr.io/iconic-exchange-272308/xyz/$*:$(VERSION)

ifeq ($(findstring dirty,$(VERSION)),)
release: release-api release-users release-projects

release-%:
	docker tag gcr.io/iconic-exchange-272308/xyz/$*:$(VERSION) gcr.io/iconic-exchange-272308/xyz/$*:latest
	docker push gcr.io/iconic-exchange-272308/xyz/$*:$(VERSION)
endif
