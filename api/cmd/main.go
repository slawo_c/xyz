package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"google.golang.org/grpc"

	pb "gitlab.com/slawo_c/xyz/api/internal/generated"
	"gitlab.com/slawo_c/xyz/xyzlib"
)

var (
	// appVersion is the application version as assigned during the build process or "dev"
	appVersion = "dev"
)

func main() {
	app := cli.App{
		Name:    "projects-svc",
		Version: appVersion,
		Action:  apiService,
		Flags: append(
			[]cli.Flag{
				&cli.StringFlag{
					Name:    "users-api",
					EnvVars: []string{"USERS_API"},
					Usage:   "the address of the users service",
					Value:   "users:9090",
				},
				&cli.StringFlag{
					Name:    "projects-api",
					EnvVars: []string{"PROJECTS_API"},
					Usage:   "the address of the projects service",
					Value:   "projects:9090",
				},
				&cli.StringFlag{
					Name:    "path-to-swagger",
					EnvVars: []string{"SWAGGER_FILE"},
					Usage:   "",
					Value:   "/www/swagger.json",
				},
				&cli.StringFlag{
					Name:    "path-to-swagger-ui",
					EnvVars: []string{"SWAGGER_UI"},
					Usage:   "",
					Value:   "/www",
				},
			},
			xyzlib.ServiceFlags...,
		),
	}

	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal("Application failed.")
	}
}

var apiService = func(c *cli.Context) error {

	if err := xyzlib.InitLogger(c.String("level")); err != nil {
		return fmt.Errorf("failed initialising service: %w", err)
	}
	ctx := context.Background()
	port := c.String("port")
	if port == "" {
		port = "8080"
		log.Printf("Defaulting to port %s", port)
	}

	mux := http.NewServeMux()
	uMux := runtime.NewServeMux()

	dopts := []grpc.DialOption{grpc.WithInsecure()}

	if c.String("users-api") != "" {
		logrus.WithField("users_api", c.String("users-api")).Debug("Mounting users.")
		err := pb.RegisterUsersServiceHandlerFromEndpoint(ctx, uMux, c.String("users-api"), dopts)
		if err != nil {
			return err
		}
	}

	if c.String("projects-api") != "" {
		logrus.WithField("projects_api", c.String("projects-api")).Debug("Mounting projects.")
		err := pb.RegisterProjectsServiceHandlerFromEndpoint(ctx, uMux, c.String("projects-api"), dopts)
		if err != nil {
			return err
		}
	}

	logrus.WithField("path", c.String("path-to-swagger")).Debug("Serving swagger file.")
	mux.HandleFunc("/swagger.json", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, c.String("path-to-swagger"))
	})
	logrus.WithField("path", c.String("path-to-swagger-ui")).Debug("Serving swagger ui.")
	fs := http.FileServer(http.Dir(c.String("path-to-swagger-ui")))
	mux.Handle("/swagger-ui/", http.StripPrefix("/swagger-ui", fs))

	mux.Handle("/v1/", uMux)
	mux.HandleFunc("/", indexHandler)

	log.Printf("Listening on port %s", port)
	return http.ListenAndServe(":"+port, mux)
}

// indexHandler used as quick and dirty ready endpoint (return 200).
func indexHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}
	fmt.Fprint(w, "OK")
}
