package main

import (
	"errors"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/slawo_c/xyz/projects/internal/projects"
	"gitlab.com/slawo_c/xyz/xyzlib"
	"gitlab.com/slawo_c/xyz/xyzlib/db"
)

var (
	// appVersion is the application version as assigned during the build process or "dev"
	appVersion = "dev"
)

var projectsService = func(c *cli.Context) error {
	var pStore projects.Store

	if c.String("db-host") != "" {
		sql, err := db.NewPSQL(
			c.String("db-host"),
			c.Int("db-port"),
			c.String("db-database"),
			c.String("db-user"),
			c.String("db-password"),
			false,
		)
		if err != nil {
			return err
		}
		defer sql.Close()
		pStore, err = newSQLStore(sql)
		if err != nil {
			return err
		}
	} else {
		return errors.New("missing a valid data connection")
	}

	ctl := projects.NewController(pStore)

	return xyzlib.RunService(c, serviceRegistration(ctl))
}

func main() {
	app := cli.App{
		Name:    "projects-svc",
		Version: appVersion,
		Action:  projectsService,
		Flags: append(
			[]cli.Flag{
				db.HostFLag,
				db.PortFLag,
				db.DBFLag,
				db.UserFLag,
				db.PasswordFLag,
				xyzlib.AuthFLag,
			},
			xyzlib.ServiceFlags...,
		),
	}

	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal("Application failed.")
	}
}
