package main

import (
	"context"

	"github.com/sirupsen/logrus"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	pb "gitlab.com/slawo_c/xyz/projects/internal/generated"
	"gitlab.com/slawo_c/xyz/projects/internal/projects"
	"gitlab.com/slawo_c/xyz/xyzlib/auth"
)

func serviceRegistration(ctl *projects.Controller) func(ctx context.Context, grpcServer *grpc.Server) error {
	return func(
		ctx context.Context,
		grpcServer *grpc.Server,
	) error {
		logrus.Debug("Registering service to grpc")
		pb.RegisterProjectsServiceServer(grpcServer, newService(ctl))
		return nil
	}
}

type projectService struct {
	ctl *projects.Controller
}

func newService(ctl *projects.Controller) *projectService {
	logrus.Debug("new service")
	return &projectService{
		ctl: ctl,
	}
}

func (s *projectService) CreateProject(ctx context.Context, r *pb.ProjectCreationRequest) (*pb.ProjectCreationResponse, error) {
	t := auth.AuthToken(ctx)
	if t == nil {
		return nil, status.Errorf(codes.Unauthenticated, "missing a valid user")
	}
	project := &projects.Project{Name: r.Name, OwnerID: t.GetUserID()}
	p, err := s.ctl.CreateProject(ctx, project)
	if err != nil {
		switch err {
		case projects.ErrMissingProjectName, projects.ErrMissingUserID:
			return nil, status.Errorf(codes.FailedPrecondition, "failed creating a project: %v", err)
		default:
			logrus.WithError(err).WithContext(ctx).Error("failed creating a project")
			return nil, status.Errorf(codes.Internal, "failed creating a project")
		}
	}
	return &pb.ProjectCreationResponse{Project: &pb.Project{Id: p.ID, HasAccess: p.OwnerID == t.GetUserID()}}, nil
}

func (s *projectService) ListProjects(ctx context.Context, r *pb.ListProjectsRequest) (*pb.ListProjectsResponse, error) {
	t := auth.AuthToken(ctx)
	if t == nil {
		return nil, status.Errorf(codes.Unauthenticated, "missing a valid user")
	}

	pp, err := s.ctl.ListProjects(ctx, t.GetUserID())
	if err != nil {
		switch err {
		case projects.ErrMissingUserID:
			return nil, status.Errorf(codes.FailedPrecondition, "failed listing projects: %v", err)
		default:
			logrus.WithError(err).WithContext(ctx).Error("failed listing projects")
			return nil, status.Errorf(codes.Internal, "failed listing projects")
		}
	}

	res := make([]*pb.Project, 0, len(pp))
	for _, p := range pp {
		res = append(res, &pb.Project{Id: p.ID, Name: p.Name, HasAccess: p.OwnerID == t.GetUserID()})
	}
	return &pb.ListProjectsResponse{Project: res}, nil
}

func (s *projectService) GetProject(ctx context.Context, r *pb.GetProjectRequest) (*pb.GetProjectResponse, error) {
	t := auth.AuthToken(ctx)
	if t == nil {
		return nil, status.Errorf(codes.Unauthenticated, "missing a valid user")
	}
	p, err := s.ctl.GetProject(ctx, t.GetUserID(), r.ProjectId)

	if err != nil {
		switch err {
		case projects.ErrInvalidProjectID, projects.ErrMissingUserID:
			return nil, status.Errorf(codes.FailedPrecondition, "failed retrieving project: %v", err)
		case projects.ErrNotPermited:
			return nil, status.Errorf(codes.PermissionDenied, "failed retrieving project: %v", err)
		case projects.ErrProjectNotFound:
			return nil, status.Errorf(codes.NotFound, "failed retrieving project: %v", err)
		default:
			logrus.WithError(err).WithContext(ctx).Error("failed retrieving project")
			return nil, status.Errorf(codes.Internal, "failed retrieving project")
		}
	}

	return &pb.GetProjectResponse{
		Id:   p.ID,
		Name: p.Name,
	}, nil
}

func (s *projectService) AddUser(ctx context.Context, r *pb.ProjectAddUserRequest) (*pb.ProjectAddUserResponse, error) {
	t := auth.AuthToken(ctx)
	if t == nil {
		return nil, status.Errorf(codes.Unauthenticated, "missing a valid user")
	}

	if err := s.ctl.AddUserToProject(ctx, t.GetUserID(), r.ProjectId, r.UserId); err != nil {
		switch err {
		case projects.ErrInvalidProjectID, projects.ErrMissingUserID:
			return nil, status.Errorf(codes.FailedPrecondition, "failed adding user: %v", err)
		case projects.ErrNotPermited:
			return nil, status.Errorf(codes.PermissionDenied, "failed adding user: %v", err)
		case projects.ErrProjectNotFound:
			return nil, status.Errorf(codes.NotFound, "failed adding user: %v", err)
		default:
			logrus.WithError(err).WithContext(ctx).Error("failed adding user")
			return nil, status.Errorf(codes.Internal, "failed adding user")
		}
	}
	return &pb.ProjectAddUserResponse{}, nil
}

func (s *projectService) AddDevice(ctx context.Context, r *pb.ProjectAddDeviceRequest) (*pb.ProjectAddDeviceResponse, error) {
	t := auth.AuthToken(ctx)
	if t == nil {
		return nil, status.Errorf(codes.Unauthenticated, "missing a valid user")
	}

	if err := s.ctl.AddDeviceToProject(ctx, t.GetUserID(), r.ProjectId, r.DeviceId); err != nil {
		switch err {
		case projects.ErrInvalidProjectID, projects.ErrInvalidDeviceID:
			return nil, status.Errorf(codes.FailedPrecondition, "failed adding device: %v", err)
		case projects.ErrNotPermited:
			return nil, status.Errorf(codes.PermissionDenied, "failed adding device: %v", err)
		case projects.ErrProjectNotFound:
			return nil, status.Errorf(codes.NotFound, "failed adding device: %v", err)
		default:
			logrus.WithError(err).WithContext(ctx).Error("failed adding device")
			return nil, status.Errorf(codes.Internal, "failed adding device")
		}
	}
	return &pb.ProjectAddDeviceResponse{}, nil
}

func (s *projectService) AddNewDevice(ctx context.Context, r *pb.ProjectAddNewDeviceRequest) (*pb.ProjectAddNewDeviceResponse, error) {
	t := auth.AuthToken(ctx)
	if t == nil {
		return nil, status.Errorf(codes.Unauthenticated, "missing a valid user")
	}

	d := &projects.Device{ID: r.SerialNumber, Name: r.Name}
	if err := s.ctl.AddNewDevice(ctx, t.GetUserID(), r.ProjectId, d); err != nil {
		switch err {
		case projects.ErrInvalidProjectID, projects.ErrInvalidDeviceID:
			return nil, status.Errorf(codes.FailedPrecondition, "failed adding device: %v", err)
		case projects.ErrNotPermited:
			return nil, status.Errorf(codes.PermissionDenied, "failed adding device: %v", err)
		case projects.ErrProjectNotFound:
			return nil, status.Errorf(codes.NotFound, "failed adding device: %v", err)
		default:
			logrus.WithError(err).WithContext(ctx).Error("failed adding device")
			return nil, status.Errorf(codes.Internal, "failed adding device")
		}
	}
	return &pb.ProjectAddNewDeviceResponse{}, nil
}
