package main

import (
	"context"
	"database/sql"
	"errors"

	"github.com/sirupsen/logrus"

	"gitlab.com/slawo_c/xyz/projects/internal/projects"
)

func newSQLStore(db *sql.DB) (projects.Store, error) {
	//TODO: use FK on owner_id
	//TODO: remove devices from this store/service when necessary
	create := `
	CREATE TABLE IF NOT EXISTS projects (
		id VARCHAR(36) PRIMARY KEY,
		name TEXT NOT NULL,
		owner_id VARCHAR(36) NOT NULL
	);
	CREATE TABLE IF NOT EXISTS projects_users (
		project_id VARCHAR(36),
		user_id VARCHAR(36),
		PRIMARY KEY (project_id, user_id)
	);
	CREATE TABLE IF NOT EXISTS projects_devices (
		project_id VARCHAR(36),
		device_id VARCHAR(5),
		PRIMARY KEY (project_id, device_id)
	);
	CREATE TABLE IF NOT EXISTS devices (
		id VARCHAR(5),
		name TEXT NOT NULL,
		PRIMARY KEY (id)
	);
	`
	_, err := db.Exec(create)
	if err != nil {
		return nil, err
	}

	return &sqlStore{db: db}, nil
}

type sqlStore struct {
	db *sql.DB
}

func (s *sqlStore) AddProject(ctx context.Context, p *projects.Project) (*projects.Project, error) {
	statement := `
	INSERT INTO projects (id, name, owner_id)
	VALUES ($1, $2, $3);`

	logrus.WithField("id", p.ID).WithField("name", p.Name).WithField("owner_id", p.OwnerID).Debug("Adding project")

	_, err := s.db.Exec(statement, p.ID, p.Name, p.OwnerID)
	if err != nil {
		return nil, err
	}
	return p, nil
}

func (s *sqlStore) GetProject(ctx context.Context, projectID string) (*projects.Project, error) {
	statement := `
	SELECT id, name, owner_id FROM projects WHERE id=$1`

	row := s.db.QueryRow(statement, projectID)
	var id string
	var name string
	var ownerID string
	if err := row.Scan(&id, &name, &ownerID); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}
	return &projects.Project{
		ID:      id,
		Name:    name,
		OwnerID: ownerID,
	}, nil
}

func (s *sqlStore) AddProjectUser(ctx context.Context, projectID, userID string) error {
	statement := `
	INSERT INTO projects_users (project_id, user_id)
	VALUES ($1, $2);`

	_, err := s.db.Exec(statement, projectID, userID)
	return err
}

func (s *sqlStore) VeryfyProjectUser(ctx context.Context, projectID, userID string) (bool, error) {
	statement := `
	SELECT name, owner_id FROM projects_users id`

	row := s.db.QueryRow(statement, projectID, userID)
	var id string
	var name string
	var ownerID string
	if err := row.Scan(&id, &name, &ownerID); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

func (s *sqlStore) GetUserProjects(ctx context.Context, userID string) ([]*projects.Project, error) {
	statement := `
	SELECT id, name, owner_id FROM projects
	WHERE owner_id=$1
	OR id=(SELECT project_id AS id FROM projects_users WHERE user_id=$1);`

	rows, err := s.db.Query(statement, userID)
	if nil != err {
		return nil, err
	}
	defer rows.Close()
	return scanProjects(rows)
}

func (s *sqlStore) GetProjectUsers(ctx context.Context, projectID string) ([]string, error) {
	statement := `
	SELECT user_id FROM projects_users
	WHERE project_id=$1;`

	rows, err := s.db.Query(statement, projectID)
	if nil != err {
		return nil, err
	}
	defer rows.Close()
	return scanIDs(rows)
}

func (s *sqlStore) CreateDevice(tx context.Context, device *projects.Device) error {
	statement := `
	INSERT INTO devices (id, name)
	VALUES ($1, $2);`

	_, err := s.db.Exec(statement, device.ID, device.Name)
	return err
}

func (s *sqlStore) AddProjectDevice(ctx context.Context, projectID, deviceID string) error {
	statement := `
	INSERT INTO projects_devices (project_id, device_id)
	VALUES ($1, $2);`

	_, err := s.db.Exec(statement, projectID, deviceID)
	return err
}

func (s *sqlStore) GetProjectDevices(ctx context.Context, projectID string) ([]*projects.Device, error) {
	statement := `
	SELECT id, name FROM devices d
	INER JOIN projects_devices pd ON pd.project_id=d.id
	WHERE pd.project_id $1;`

	rows, err := s.db.Query(statement, projectID)
	if nil != err {
		return nil, err
	}
	defer rows.Close()

	res := []*projects.Device{}

	for rows.Next() {
		var id string
		var name string
		err := rows.Scan(&id, &name)
		if err != nil {
			return nil, err
		}
		res = append(res, &projects.Device{ID: id, Name: name})
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return res, nil
}

func scanProjects(rows *sql.Rows) ([]*projects.Project, error) {
	res := []*projects.Project{}

	for rows.Next() {
		var id string
		var name string
		var ownerID string
		err := rows.Scan(&id, &name, &ownerID)
		if err != nil {
			return nil, err
		}
		res = append(res, &projects.Project{ID: id, Name: name, OwnerID: ownerID})

	}
	err := rows.Err()
	if err != nil {
		return nil, err
	}

	return res, nil
}

func scanIDs(rows *sql.Rows) ([]string, error) {
	res := []string{}

	for rows.Next() {
		var id string
		err := rows.Scan(&id)
		if err != nil {
			return nil, err
		}
		res = append(res, id)

	}
	err := rows.Err()
	if err != nil {
		return nil, err
	}

	return res, nil
}
