package projects

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	// ErrProjectNotFound project doesn't exist
	ErrProjectNotFound = errors.New("project not found")
	// ErrNotPermited project operation is not available to the given user
	ErrNotPermited = errors.New("project not available")
	// ErrInvalidProjectID project id is invalid or missing
	ErrInvalidProjectID = errors.New("invalid project id")
	// ErrMissingProjectName project name is missing
	ErrMissingProjectName = errors.New("missing project name")
	// ErrMissingUserID user id is missing
	ErrMissingUserID     = errors.New("missing user id")
	ErrInvalidDeviceID   = errors.New("invalid device id")
	ErrInvalidDeviceName = errors.New("invalid device name")
)

func NewController(store Store) *Controller {
	return &Controller{store: store}
}

type Controller struct {
	store Store
}

// CreateProject adds a new project to the list
func (c *Controller) CreateProject(ctx context.Context, p *Project) (*Project, error) {
	logrus.WithField("name", p.Name).WithField("owner_id", p.OwnerID).Debug("CreateProject")
	if p.ID != "" {
		return nil, ErrInvalidProjectID
	}
	if p.OwnerID == "" {
		return nil, ErrMissingUserID
	}
	if p.Name == "" {
		return nil, ErrMissingProjectName
	}
	project := *p
	project.ID = uuid.New().String()

	return c.store.AddProject(ctx, &project)
}

func (c *Controller) GetProject(ctx context.Context, userId, projectID string) (*Project, error) {
	logrus.WithField("id", projectID).WithField("user_id", userId).Debug("GetProject")
	if projectID == "" {
		return nil, ErrInvalidProjectID
	}
	if userId == "" {
		return nil, ErrMissingUserID
	}
	p, err := c.store.GetProject(ctx, projectID)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed retrieving a project: %v", err)
	}
	if p == nil {
		return nil, ErrProjectNotFound
	}
	if p.OwnerID != userId {
		ok, err := c.store.VeryfyProjectUser(ctx, projectID, userId)
		if err != nil {
			return nil, fmt.Errorf("failed verifying user: %w", err)
		}
		if !ok {
			return nil, ErrNotPermited
		}
	}
	return p, nil
}

func (c *Controller) ListProjects(ctx context.Context, userID string) ([]*Project, error) {
	if userID == "" {
		return nil, ErrMissingUserID
	}
	logrus.WithField("user", userID).Debug("ListProjects")
	ps, err := c.store.GetUserProjects(ctx, userID)
	if err != nil {
		return nil, fmt.Errorf("failed listing projects: %w", err)
	}
	return ps, nil
}

func (c *Controller) AddUserToProject(ctx context.Context, userID, projectID, newUserID string) error {
	if userID == "" {
		return ErrNotPermited
	}
	if newUserID == "" {
		return ErrMissingUserID
	}
	if projectID == "" {
		return ErrInvalidProjectID
	}
	if newUserID == userID {
		return errors.New("user icannot add himself")
	}

	p, err := c.store.GetProject(ctx, projectID)
	if err != nil {
		return err
	}
	if p == nil {
		return ErrProjectNotFound
	}
	if p.OwnerID != userID {
		return ErrNotPermited
	}
	return c.store.AddProjectUser(ctx, projectID, newUserID)
}

func (c *Controller) AddDeviceToProject(ctx context.Context, userID, projectID, newDeviceID string) error {
	if userID == "" {
		return ErrNotPermited
	}
	if projectID == "" {
		return ErrInvalidProjectID
	}
	if newDeviceID == "" {
		return ErrInvalidDeviceID
	}

	p, err := c.store.GetProject(ctx, projectID)
	if err != nil {
		return err
	}
	if p == nil {
		return ErrProjectNotFound
	}
	if p.OwnerID != userID {
		return ErrNotPermited
	}
	return c.store.AddProjectDevice(ctx, projectID, newDeviceID)
}

func (c *Controller) AddNewDevice(ctx context.Context, userID, projectID string, newDevice *Device) error {
	if userID == "" {
		return ErrNotPermited
	}
	if projectID == "" {
		return ErrInvalidProjectID
	}
	if newDevice.ID == "" {
		return ErrInvalidDeviceID
	}
	if newDevice.Name == "" {
		return ErrInvalidDeviceName
	}

	p, err := c.store.GetProject(ctx, projectID)
	if err != nil {
		return err
	}
	if p == nil {
		return ErrProjectNotFound
	}
	if p.OwnerID != userID {
		return ErrNotPermited
	}

	//TODO: either merge the 2 DB acesses to 1 OP or split the struct
	err = c.store.CreateDevice(ctx, newDevice)
	if err != nil {
		return err
	}

	return c.store.AddProjectDevice(ctx, projectID, newDevice.ID)
}
