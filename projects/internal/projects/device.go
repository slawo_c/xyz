package projects

// Device represents a hardware device.
type Device struct {
	ID   string
	Name string
}
