package projects

import (
	"context"
)

// Project represents a project
type Project struct {
	ID      string
	Name    string
	OwnerID string
}

// Store represents a Project store
type Store interface {
	AddProject(ctx context.Context, project *Project) (*Project, error)
	GetProject(ctx context.Context, projectID string) (*Project, error)
	// TODO: commit to moving User/project functions to a different service
	AddProjectUser(ctx context.Context, projectID, userID string) error
	VeryfyProjectUser(ctx context.Context, projectID, userID string) (bool, error)
	GetUserProjects(ctx context.Context, userID string) ([]*Project, error)
	// TODO: commit to moving Device/project functions to a different service
	CreateDevice(tx context.Context, device *Device) error
	AddProjectDevice(ctx context.Context, projectID, deviceID string) error
	GetProjectDevices(ctx context.Context, projectID string) ([]*Device, error)
}
