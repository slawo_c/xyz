package auth

import "errors"

var (
	ErrAuth         = errors.New("authentication error")
	ErrInvalidToken = errors.New("invalid token")
)

// Token represents the company wide abstraction of an authentication token
// we purposefully ignore all aspects outside of the identity (scopes etc...)
type Token interface {
	GetUserID() string
	Token() string
}
