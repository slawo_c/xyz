package auth

import (
	"strings"
)

// NewAuthenticator instantiates a new authenticator
func NewAuthenticator() (Authenticator, error) {
	return AuthenticatorFunc(func(data string) (Token, error) {
		// validate the token
		if !strings.HasPrefix(data, "Bearer ") {
			return nil, ErrInvalidToken
		}
		t := tmpToken{token: data}
		if t.GetUserID() == "" {
			return nil, ErrInvalidToken
		}

		return &t, nil

	}), nil
}

// NewIssuer instantiates a new issuer
func NewIssuer() (Issuer, error) {
	return IssuerFunc(func(sub string) (Token, error) {
		return &tmpToken{token: "Bearer " + sub}, nil
	}), nil
}

type tmpToken struct {
	token string
}

func (t *tmpToken) GetUserID() string {
	return strings.TrimPrefix(strings.TrimSpace(t.token), "Bearer ")
}

func (t *tmpToken) Token() string {
	return t.token
}
