package auth

// Authenticator represents a Token authenticator. Its aim is to take auth
// data and return a token which can be usd by the application
type Authenticator interface {
	VerifyToken(string) (Token, error)
}

// AuthenticatorFunc is a elper to use functions as Authenticator
type AuthenticatorFunc func(string) (Token, error)

// VerifyToken calls the AuthenticatorFunc and returns its result
func (f AuthenticatorFunc) VerifyToken(data string) (Token, error) {
	return f(data)
}
