package auth

import (
	"context"
	"errors"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type ContextAuthenticator func(ctx context.Context) (Token, error)

func NewContextAuthenticator(auth Authenticator) ContextAuthenticator {
	return func(ctx context.Context) (Token, error) {
		if nil == auth {
			return nil, errors.New("missing authenticator")
		}
		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return nil, errors.New("missing headers")
		}
		tokens := md.Get("authorization")

		logrus.Debug("Authenticator")
		if len(tokens) == 0 {
			return nil, errors.New("missing token")
		}
		if len(tokens) != 1 {
			return nil, errors.New("invalif tokens count")
		}

		return auth.VerifyToken(tokens[0])
	}
}

type contextKey string

var (
	contextKeyAuthtoken = contextKey("auth-token")
)

func AuthToken(ctx context.Context) Token {
	tokenStr, ok := ctx.Value(contextKeyAuthtoken).(Token)
	if !ok {
		return nil
	}
	return tokenStr
}
func NewUnaryInterceptor(auth Authenticator) func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	ctxAuth := NewContextAuthenticator(auth)
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		logrus.Debug("Authenticator")
		token, err := ctxAuth(ctx)
		if err != nil {
			return nil, err
		}

		ctx = context.WithValue(ctx, contextKeyAuthtoken, token)

		m, err := handler(ctx, req)
		if err != nil {
			logrus.WithError(err).Error("rpc failed")
		}
		return m, err
	}
}
