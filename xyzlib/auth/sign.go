package auth


// Issuer Issuer represents a token issuer (should be used in only one place).
type Issuer interface {
	Generate(string) (Token, error)
}

// IssuerFunc is a helper to use functions as Issuer
type IssuerFunc func(string) (Token, error)

// Generate calls the IssuerFunc and returns its result
func (f IssuerFunc) Generate(data string) (Token, error) {
	return f(data)
}
