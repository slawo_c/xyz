package xyzlib

import (
	"context"
	"fmt"
	"log"
	"net"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/slawo_c/xyz/xyzlib/auth"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	portFLag = &cli.StringFlag{
		Name:     "port, p",
		EnvVars:  []string{"PORT"},
		Required: true,
		Usage:    "the port of the service",
	}
	levelFLag = &cli.StringFlag{
		Name:    "level, l",
		EnvVars: []string{"LEVEL"},
		Usage:   "the reporting level of the application [error|warn|info|debug]",
		Value:   "error",
	}
	AuthFLag = &cli.StringFlag{
		Name:    "auth-key",
		EnvVars: []string{"AUTH_KEY"},
		Usage:   "the key to use for authentication",
	}
	ServiceFlags = []cli.Flag{
		portFLag,
		levelFLag,
	}
)

// RunService initialises a standard microservice and runs the given rpc service
// TODO: move this to a custom configurable proto generator to avoid the serviceInit nonsense
func RunService(
	c *cli.Context,
	serviceInit func(context.Context, *grpc.Server) error,
) error {
	ctx := context.Background() // TODO: use a better sig-aware context

	if err := InitLogger(c.String("level")); err != nil {
		return fmt.Errorf("failed initialising service: %w", err)
	}

	opts := []grpc.ServerOption{}

	logrus.WithField("auth_key", c.String("auth-key")).Debug("setup")

	if c.String("auth-key") != "" {
		uAuth, err := auth.NewAuthenticator()
		if err != nil {
			return err
		}
		i := auth.NewUnaryInterceptor(uAuth)
		opts = append(opts, grpc.UnaryInterceptor(i))
	}

	grpcS := grpc.NewServer(opts...)

	if c.Bool("service-reflect") {
		reflection.Register(grpcS)
	}

	if err := serviceInit(ctx, grpcS); err != nil {
		return fmt.Errorf("failed initialising service: %w", err)
	}

	port := c.String("port")
	logrus.WithField("port", port).Info("Sarting service")
	lis, err := net.Listen("tcp", ":"+port)
	if err != nil {
		return fmt.Errorf("failed to listen: %w", err)
	}
	err = grpcS.Serve(lis)
	logrus.Info("Service stopped.")
	return err
}

func RunServer(
	ctx context.Context,
	addr string,
	grpcS *grpc.Server,
) error {

	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	return grpcS.Serve(lis)
}
