package xyzlib

import (
	"strings"

	"github.com/sirupsen/logrus"
)

func InitLogger(level string) error {
	lvl, err := logrus.ParseLevel(strings.ToLower(level))
	if err != nil {
		return err
	}
	logrus.SetLevel(lvl)
	logrus.Info("Log level set to " + strings.ToLower(level))
	return nil
}
