package db

import (
	"database/sql"
	"fmt"

	"github.com/sirupsen/logrus"

	"github.com/lib/pq"
)

// IsDuplicateError checks for a duplicate error
func IsDuplicateError(err error) bool {
	if pgerr, ok := err.(*pq.Error); ok {
		if pgerr.Code == "23505" {
			return true
		}
	}
	return false
}

// NewPSQL returns a new instance of postgresql
func NewPSQL(host string, port int, dbname, username, password string, sslMode bool) (*sql.DB, error) {
	logrus.
		WithField("host", host).
		WithField("port", port).
		WithField("dbname", dbname).
		WithField("username", username).
		WithField("sslMode", sslMode).
		Info("Connecting to postgres.")

	psqlInfo := NewPSQLInfo(host, port, dbname, username, password, sslMode)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, err
	}
	err = db.Ping()

	if err != nil {
		return nil, err
	}
	logrus.Info("Connected to postgres.")
	return db, nil
}

// NewPSQLInfo creates a psql info string
func NewPSQLInfo(host string, port int, dbname, username, password string, sslMode bool) string {

	psqlInfo := fmt.Sprintf("host=%s", host)
	if port != 0 {
		psqlInfo += fmt.Sprintf(" port=%d", port)
	}
	if username != "" {
		psqlInfo += fmt.Sprintf(" user=%s", username)
	}
	if password != "" {
		psqlInfo += fmt.Sprintf(" password=%s", password)
	}
	if dbname != "" {
		psqlInfo += fmt.Sprintf(" dbname=%s", dbname)
	}
	if !sslMode {
		psqlInfo += " sslmode=disable"
	}

	return psqlInfo
}
