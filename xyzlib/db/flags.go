package db

import "github.com/urfave/cli/v2"

var (
	HostFLag = &cli.StringFlag{
		Name:    "db-host",
		EnvVars: []string{"DB_HOST"},
		Usage:   "the database host",
	}
	PortFLag = &cli.StringFlag{
		Name:    "db-port",
		EnvVars: []string{"DB_PORT"},
		Usage:   "the database port",
	}
	DBFLag = &cli.StringFlag{
		Name:    "db-database",
		EnvVars: []string{"DB_DATABASE"},
		Usage:   "the database to use",
	}
	UserFLag = &cli.StringFlag{
		Name:    "db-user",
		EnvVars: []string{"DB_USER"},
		Usage:   "the database username",
	}
	PasswordFLag = &cli.StringFlag{
		Name:    "db-password",
		EnvVars: []string{"DB_PASSWORD"},
		Usage:   "the database password",
	}
)
