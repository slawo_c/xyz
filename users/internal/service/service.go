package service

import (
	"context"
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	pb "gitlab.com/slawo_c/xyz/users/internal/generated"
	"gitlab.com/slawo_c/xyz/users/internal/users"
)

// ServiceRegistration
func ServiceRegistration(ctl *users.Controller) func(context.Context, *grpc.Server) error {
	return func(
		ctx context.Context,
		grpcServer *grpc.Server,
	) error {
		pb.RegisterUsersServiceServer(grpcServer, newServer(ctl))
		return nil
	}
}

type userService struct {
	ctl *users.Controller
}

func (s *userService) RegisterUser(ctx context.Context, req *pb.UserRegistrationRequest) (*pb.UserRegistrationResponse, error) {
	fmt.Printf("rpc registering user (%s, %s)\n", req.GetFirstName(), req.GetLastName())
	user, err := s.ctl.Register(ctx, &users.User{Email: req.Email, FirstName: req.FirstName, LastName: req.LastName}, req.Password)
	if err != nil {
		if errors.Is(err, users.ErrInvalidUserID) ||
			errors.Is(err, users.ErrMissingEmail) ||
			errors.Is(err, users.ErrMissingPassword) {
			return nil, status.Errorf(codes.InvalidArgument, "%v", err)
		}
		if errors.Is(err, users.ErrUserExists) {
			return nil, status.Errorf(codes.AlreadyExists, "%v", err)
		}
		// Unexpected error
		logrus.WithError(err).Error("RegisterUser")
		return nil, err
	}
	return &pb.UserRegistrationResponse{UserId: user.ID}, nil
}

func (s *userService) Authenticate(ctx context.Context, req *pb.UserAuthenticationRequest) (*pb.UserAuthenticationResponse, error) {
	token, err := s.ctl.Login(ctx, req.Email, req.Password)
	if err != nil {
		if errors.Is(err, users.ErrMissingEmail) ||
			errors.Is(err, users.ErrMissingPassword) {
			return nil, status.Errorf(codes.InvalidArgument, "%v", err)
		}
		if errors.Is(err, users.ErrMissingCredentials) ||
			errors.Is(err, users.ErrInvalidCredentials) {
			// do not return the specific error
			return nil, status.Error(codes.Unauthenticated, "login failed")
		}
		// Unexpected error
		logrus.WithError(err).Error("Authenticate")
		return nil, err
	}
	return &pb.UserAuthenticationResponse{Token: token.Token()}, nil
}

func newServer(ctl *users.Controller) *userService {
	return &userService{
		ctl: ctl,
	}
}
