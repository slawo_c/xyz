package users

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/slawo_c/xyz/xyzlib/auth"
	"golang.org/x/crypto/bcrypt"
)

var (
	// ErrMissingUserDAO returned when a component is missing
	ErrMissingUserDAO = errors.New("missing users dao")
	// ErrUserExists returned when trying to register an existing user
	ErrUserExists = errors.New("user already exists")
	// ErrNilUser returned when a nil object is passed instead of the expectd user
	ErrNilUser = errors.New("nil user")
	// ErrInvalidUserID returned when an unexpected user id is returned
	ErrInvalidUserID = errors.New("invalid user id")
	// ErrMissingEmail returned when an expected email is missing
	ErrMissingEmail = errors.New("missing email")
	// ErrMissingPassword returned when an expected password is missing
	ErrMissingPassword = errors.New("missing password")
	// ErrMissingCredentials represents a generic login failure message
	ErrMissingCredentials = errors.New("missing credentials")
	// ErrInvalidCredentials represents a generic login failure message
	ErrInvalidCredentials = errors.New("invalid credentials")
)

var (
	namespace = uuid.NewSHA1(uuid.Nil, []byte("xyz-user-account"))
)

// NewController Initialises a new controller
func NewController(store Store, auth auth.Issuer) (*Controller, error) {
	if nil == store {
		return nil, ErrMissingUserDAO
	}
	return &Controller{
		users: store,
		auth:  auth,
	}, nil
}

// Controller implements the business logics for users handling.
// This implementation conflates 2 different aspects (authentication and profile)
type Controller struct {
	users Store
	auth  auth.Issuer
}

// Register registers a new user.
func (c *Controller) Register(ctx context.Context, user *User, password string) (*User, error) {
	if user == nil {
		return nil, ErrNilUser
	}
	if user.ID != "" {
		return nil, ErrInvalidUserID
	}
	if user.Email == "" {
		return nil, ErrMissingEmail
	}
	if password == "" {
		return nil, ErrMissingPassword
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, fmt.Errorf("user registration failed: %w", err)
	}

	u := *user
	u.ID = uuid.NewSHA1(namespace, []byte(user.Email)).String()

	err = c.users.AddUser(ctx, &u, string(hash))
	if err != nil {
		return nil, fmt.Errorf("user registration failed: %w", err)
	}

	return &u, nil
}

// Login authenticates a user using a username/password combination.
func (c *Controller) Login(ctx context.Context, userName string, password string) (auth.Token, error) {
	if userName == "" {
		return nil, ErrMissingEmail
	}
	if password == "" {
		return nil, ErrMissingPassword
	}

	// TODO: this is a brittle implementation of getUserID(email)
	id := uuid.NewSHA1(namespace, []byte(userName)).String()
	creds, err := c.users.GetUserCredentials(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("failed retrieving user credentials: %w", err)
	}

	if creds == "" {
		return nil, ErrMissingCredentials
	}

	err = bcrypt.CompareHashAndPassword([]byte(creds), []byte(password))
	if err != nil {
		return nil, ErrInvalidCredentials
	}

	token, err := c.auth.Generate(id)
	if err != nil {
		return nil, fmt.Errorf("user login failed: %w", err)
	}

	return token, nil
}
