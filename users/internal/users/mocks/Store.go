// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
	users "gitlab.com/slawo_c/xyz/users/internal/users"
)

// Store is an autogenerated mock type for the Store type
type Store struct {
	mock.Mock
}

// AddUser provides a mock function with given fields: _a0, _a1, _a2
func (_m *Store) AddUser(_a0 context.Context, _a1 *users.User, _a2 string) error {
	ret := _m.Called(_a0, _a1, _a2)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *users.User, string) error); ok {
		r0 = rf(_a0, _a1, _a2)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetUserByID provides a mock function with given fields: _a0, _a1
func (_m *Store) GetUserByID(_a0 context.Context, _a1 string) (*users.User, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *users.User
	if rf, ok := ret.Get(0).(func(context.Context, string) *users.User); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*users.User)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUserCredentials provides a mock function with given fields: _a0, _a1
func (_m *Store) GetUserCredentials(_a0 context.Context, _a1 string) (string, error) {
	ret := _m.Called(_a0, _a1)

	var r0 string
	if rf, ok := ret.Get(0).(func(context.Context, string) string); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
