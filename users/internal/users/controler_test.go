package users_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/slawo_c/xyz/users/internal/users"
	"gitlab.com/slawo_c/xyz/users/internal/users/mocks"
	"golang.org/x/crypto/bcrypt"
)

//go:generate mockery --name=Store
//go:generate mockery --name=Issuer --dir ../../../xyzlib/auth
//go:generate mockery --name=Token --dir ../../../xyzlib/auth

func TestController(t *testing.T) {
	suite.Run(t, &TestControllerSuite{})
}

type TestControllerSuite struct {
	suite.Suite
	ctl   *users.Controller
	store *mocks.Store
	auth  *mocks.Issuer
}

func (s *TestControllerSuite) SetupTest() {
	var err error
	s.store = &mocks.Store{}
	s.auth = &mocks.Issuer{}
	s.ctl, err = users.NewController(s.store, s.auth)
	s.Require().NoError(err)
}

func (s *TestControllerSuite) TearDownTest() {
	s.store.AssertExpectations(s.T())
	s.auth.AssertExpectations(s.T())
}

func (s *TestControllerSuite) TestRegisterSuccess() {
	usr := users.User{Email: "a@a.a"}
	s.store.On("AddUser", mock.AnythingOfType("*context.emptyCtx"), mock.AnythingOfType("*users.User"), mock.AnythingOfType("string")).Return(nil)

	res, err := s.ctl.Register(context.TODO(), &usr, "pass")
	s.Assert().Empty(usr.ID)
	s.Assert().NotEmpty(res.ID)
	s.Assert().NoError(err)
}

func (s *TestControllerSuite) TestRegisterTransmitsStoreErrors() {
	usr := users.User{Email: "a@a.a"}
	s.store.On("AddUser", mock.AnythingOfType("*context.emptyCtx"), mock.AnythingOfType("*users.User"), mock.AnythingOfType("string")).Return(errors.New("STORE_ERROR"))

	res, err := s.ctl.Register(context.TODO(), &usr, "pass")
	s.Assert().Empty(res)
	s.Assert().EqualError(err, "user registration failed: STORE_ERROR")
}

func (s *TestControllerSuite) TestRegisterFailsOnMissingUser() {
	res, err := s.ctl.Register(context.TODO(), nil, "pass")
	s.Assert().Empty(res)
	s.Assert().EqualError(err, "nil user")
}

func (s *TestControllerSuite) TestRegisterFailsOnMissingPassword() {
	usr := users.User{Email: "a@a.a"}

	res, err := s.ctl.Register(context.TODO(), &usr, "")
	s.Assert().Empty(res)
	s.Assert().EqualError(err, "missing password")
}

func (s *TestControllerSuite) TestRegisterFailsOnMissingEmail() {
	usr := users.User{Email: ""}

	res, err := s.ctl.Register(context.TODO(), &usr, "pass")
	s.Assert().Empty(res)
	s.Assert().EqualError(err, "missing email")
}

func (s *TestControllerSuite) TestRegisterFailsOnUserWithID() {
	usr := users.User{Email: "a@a.a", ID: "INVALID"}

	res, err := s.ctl.Register(context.TODO(), &usr, "pass")
	s.Assert().Empty(res)
	s.Assert().EqualError(err, "invalid user id")
}

var (
	password = "password"
	creds    = GenerateCreds(password)
)

func GenerateCreds(password string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}
	return string(hash)
}

func (s *TestControllerSuite) TestLoginSuccess() {
	token := mocks.Token{}
	s.store.On("GetUserCredentials", mock.AnythingOfType("*context.emptyCtx"), mock.AnythingOfType("string")).Return(creds, nil)
	s.auth.On("Generate", mock.AnythingOfType("string")).Return(&token, nil)

	res, err := s.ctl.Login(context.TODO(), "a@a.a", password)
	s.Assert().Equal(&token, res)
	s.Assert().NoError(err)
}

func (s *TestControllerSuite) TestLoginReturnErrorOnMissingUsername() {
	res, err := s.ctl.Login(context.TODO(), "", password)
	s.Assert().Nil(res)
	s.Assert().EqualError(err, "missing email")
}

func (s *TestControllerSuite) TestLoginReturnErrorOnMissingPassword() {
	res, err := s.ctl.Login(context.TODO(), "a@a.a", "")
	s.Assert().Nil(res)
	s.Assert().EqualError(err, "missing password")
}

func (s *TestControllerSuite) TestLoginTransmitsStoreErrors() {
	s.store.On("GetUserCredentials", mock.AnythingOfType("*context.emptyCtx"), mock.AnythingOfType("string")).Return("", errors.New("STORE_ERROR"))

	res, err := s.ctl.Login(context.TODO(), "a@a.a", password)
	s.Assert().Nil(res)
	s.Assert().EqualError(err, "failed retrieving user credentials: STORE_ERROR")
}

func (s *TestControllerSuite) TestLoginReturnLoginErrorOnMissingCreds() {
	s.store.On("GetUserCredentials", mock.AnythingOfType("*context.emptyCtx"), mock.AnythingOfType("string")).Return("", nil)

	res, err := s.ctl.Login(context.TODO(), "a@a.a", password)
	s.Assert().Nil(res)
	s.Assert().EqualError(err, "missing credentials")
}

func (s *TestControllerSuite) TestLoginReturnLoginErrorOnWrongCreds() {
	s.store.On("GetUserCredentials", mock.AnythingOfType("*context.emptyCtx"), mock.AnythingOfType("string")).Return(creds, nil)

	res, err := s.ctl.Login(context.TODO(), "a@a.a", "wrong_password")
	s.Assert().Nil(res)
	s.Assert().EqualError(err, "invalid credentials")
}

func (s *TestControllerSuite) TestLoginReturnLoginErrorTokenGenerationError() {
	s.store.On("GetUserCredentials", mock.AnythingOfType("*context.emptyCtx"), mock.AnythingOfType("string")).Return(creds, nil)
	s.auth.On("Generate", mock.AnythingOfType("string")).Return(nil, errors.New("TOKEN_ERROR"))

	res, err := s.ctl.Login(context.TODO(), "a@a.a", password)
	s.Assert().Nil(res)
	s.Assert().EqualError(err, "user login failed: TOKEN_ERROR")
}
