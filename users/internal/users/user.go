package users

import "context"

// User describes a user
type User struct {
	ID        string
	FirstName string
	LastName  string
	Email     string
}

// Store describes a users data access object
type Store interface {
	AddUser(context.Context, *User, string) error
	GetUserCredentials(context.Context, string) (string, error)
	GetUserByID(context.Context, string) (*User, error)
}
