package main

import (
	"errors"
	"net/http"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/slawo_c/xyz/users/internal/service"
	"gitlab.com/slawo_c/xyz/users/internal/users"
	"gitlab.com/slawo_c/xyz/xyzlib"
	"gitlab.com/slawo_c/xyz/xyzlib/auth"
	"gitlab.com/slawo_c/xyz/xyzlib/db"
)

var (
	// appVersion is the application version as assigned during the build process or "dev"
	appVersion = "dev"
)

var userService = func(c *cli.Context) error {
	var uStore users.Store
	var uAuth auth.Issuer

	if c.String("db-host") != "" {
		sql, err := db.NewPSQL(
			c.String("db-host"),
			c.Int("db-port"),
			c.String("db-database"),
			c.String("db-user"),
			c.String("db-password"),
			false,
		)
		if err != nil {
			return err
		}
		defer sql.Close()
		uStore, err = newSQLStore(sql)
		if err != nil {
			return err
		}
	} else {
		return errors.New("missing a valid data connection")
	}

	uAuth, err := auth.NewIssuer()
	if err != nil {
		return err
	}

	ctl, err := users.NewController(uStore, uAuth)
	if err != nil {
		return err
	}

	return xyzlib.RunService(c, service.ServiceRegistration(ctl))
}

func main() {
	app := cli.App{
		Name:    "users-svc",
		Version: appVersion,
		Action:  userService,
		Flags: append(
			[]cli.Flag{
				db.HostFLag,
				db.PortFLag,
				db.DBFLag,
				db.UserFLag,
				db.PasswordFLag,
			},
			xyzlib.ServiceFlags...,
		),
	}

	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal("Application failed.")
	}
}

func initHTTP() (*http.Server, error) {
	return nil, nil
}
