package main

import (
	"context"
	"database/sql"
	"errors"

	"gitlab.com/slawo_c/xyz/users/internal/users"
	"gitlab.com/slawo_c/xyz/xyzlib/db"
)

func newSQLStore(db *sql.DB) (users.Store, error) {

	create := `CREATE TABLE IF NOT EXISTS users (
		id VARCHAR(36) PRIMARY KEY,
		credentials TEXT NOT NULL,
		first_name TEXT,
		last_name TEXT,
		email TEXT UNIQUE NOT NULL
	);
	`
	_, err := db.Exec(create)
	if err != nil {
		return nil, err
	}

	return &sqlStore{db: db}, nil

}

type sqlStore struct {
	db *sql.DB
}

func (s *sqlStore) AddUser(ctx context.Context, u *users.User, password string) error {
	statement := `
	INSERT INTO users (id, email, first_name, last_name, credentials)
	VALUES ($1, $2, $3, $4, $5);`

	_, err := s.db.Exec(statement, u.ID, u.Email, u.FirstName, u.LastName, password)
	if err != nil {
		if db.IsDuplicateError(err) {
			return users.ErrUserExists
		}
		return err
	}
	return nil
}

func (s *sqlStore) GetUserCredentials(ctx context.Context, id string) (string, error) {
	statement := `SELECT credentials FROM users WHERE id=$1;`

	credentials := ""

	row := s.db.QueryRow(statement, id)
	if err := row.Scan(&credentials); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return "", nil
		}
		return "", err
	}
	return credentials, nil
}

func (s *sqlStore) GetUserByID(ctx context.Context, id string) (*users.User, error) {
	statement := `SELECT id, email, first_name, last_name FROM users WHERE id=$1;`

	row := s.db.QueryRow(statement, id)
	user := users.User{}
	if err := row.Scan(&user.ID, &user.Email, &user.FirstName, &user.LastName); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}
	return &user, nil
}
