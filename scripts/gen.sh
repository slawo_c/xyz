if ! command -v protoc-gen-grpc-gateway > /dev/null 2>&1 ; then
  go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
fi

if ! command -v protoc-gen-swagger > /dev/null 2>&1 ; then
  go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
fi

if ! command -v protoc-gen-go > /dev/null 2>&1 ; then
  go install github.com/golang/protobuf/protoc-gen-go
fi

rm -Rf users/internal/generated/
mkdir -p users/internal/generated/
protoc -I/usr/local/include -I./proto \
  -I$GOPATH/src \
  -I`go list -f '{{ .Dir }}' -m github.com/grpc-ecosystem/grpc-gateway`/third_party/googleapis \
  --go_out=plugins=grpc:./users/internal/generated/ \
  ./proto/users.proto

rm -Rf projects/internal/generated/
mkdir -p projects/internal/generated/
protoc -I/usr/local/include -I./proto \
  -I$GOPATH/src \
  -I`go list -f '{{ .Dir }}' -m github.com/grpc-ecosystem/grpc-gateway`/third_party/googleapis \
  --go_out=plugins=grpc:./projects/internal/generated/ \
  ./proto/projects.proto ./proto/devices.proto


rm -Rf api/internal/generated/
mkdir -p api/internal/generated/
protoc -I/usr/local/include -I./proto \
  -I$GOPATH/src \
  -I`go list -f '{{ .Dir }}' -m github.com/grpc-ecosystem/grpc-gateway`/third_party/googleapis \
  --go_out=plugins=grpc:./api/internal/generated/ \
  --grpc-gateway_out=logtostderr=true:./api/internal/generated/  \
  --swagger_out=logtostderr=true,allow_merge=true:./api/internal/generated/  \
  ./proto/*.proto
